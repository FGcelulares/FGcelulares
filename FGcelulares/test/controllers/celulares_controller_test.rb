require 'test_helper'

class CelularesControllerTest < ActionController::TestCase
  setup do
    @celulare = celulares(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:celulares)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create celulare" do
    assert_difference('Celulare.count') do
      post :create, celulare: { active_record: @celulare.active_record, description: @celulare.description, invoke: @celulare.invoke, location: @celulare.location, title: @celulare.title }
    end

    assert_redirected_to celulare_path(assigns(:celulare))
  end

  test "should show celulare" do
    get :show, id: @celulare
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @celulare
    assert_response :success
  end

  test "should update celulare" do
    patch :update, id: @celulare, celulare: { active_record: @celulare.active_record, description: @celulare.description, invoke: @celulare.invoke, location: @celulare.location, title: @celulare.title }
    assert_redirected_to celulare_path(assigns(:celulare))
  end

  test "should destroy celulare" do
    assert_difference('Celulare.count', -1) do
      delete :destroy, id: @celulare
    end

    assert_redirected_to celulares_path
  end
end
