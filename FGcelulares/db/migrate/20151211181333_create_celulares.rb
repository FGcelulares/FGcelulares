class CreateCelulares < ActiveRecord::Migration
  def change
    create_table :celulares do |t|
      t.string :title
      t.string :location
      t.text :description
      t.string :invoke
      t.string :active_record

      t.timestamps null: false
    end
  end
end
