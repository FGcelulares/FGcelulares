json.array!(@celulares) do |celulare|
  json.extract! celulare, :id, :title, :location, :description, :invoke, :active_record
  json.url celulare_url(celulare, format: :json)
end
