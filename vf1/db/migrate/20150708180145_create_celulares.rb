class CreateCelulares< ActiveRecord::Migration
  def change
    create_table :Celulares do |t|
      t.string :Marca
      t.string :preco
      t.string :telefone
      t.text :descricao

      t.timestamps null: false
    end
  end
end
