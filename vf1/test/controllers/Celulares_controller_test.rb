require 'test_helper'

class CelularesControllerTest < ActionController::TestCase
  setup do
    @celulares = celulares(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:celulares)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create celulares" do
    assert_difference('Celulares.count') do
      post :create, book: { marca: @Celulares.marca, descricao: @celulares.descricao, price: @Celulares.price, telefone: @Celulares.telefone}
    end

    assert_redirected_to book_path(assigns(:celulares))
  end

  test "should show celulares" do
    get :show, id: @celulares
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @celulares
    assert_response :success
  end

  test "should update celulares" do
    patch :update, id: @celulares, celulares: { marca: @celulares.marca, descricao @celulares.descricao, price: @celulares.price, telefone: @celulares.telefone }
    assert_redirected_to book_path(assigns(:celulares))
  end

  test "should destroy celulares" do
    assert_difference('Celulares.count', -1) do
      delete :destroy, id: @celulares
    end

    assert_redirected_to celulares_path
  end
end
