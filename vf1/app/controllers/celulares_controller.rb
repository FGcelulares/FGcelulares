class CelularesController < ApplicationController
	before_action :set_celulares, only: [:show]
  before_action :set_users_celulares, only: [:edit, :update, :destroy]
	before_action :require_authentication, only: [:show, :edit, :create, :update, :destroy]

  def index
    @celulares= celulares.all
  end

  def show
  end

  def new
    @celulares = celulares.new
  end

  def edit
  end

  def create
    @celulares = celulares.new(celulares_params)

      if @celulares.save
        redirect_to @celulares, notice: t('flash.notice.celulares_created')
      else
        render :new
      end
  end

  def update
      if @celulares.update(celulares_params)
        redirect_to @celulares, notice: t('flash.notice.celulares_updated')
      else
        render :edit 
      end
  end

  def destroy
    @celulares.destroy
    redirect_to books_url  
  end

  private
    def set_book
      @celulares = Celulares.find(params[:id])
    end

    def book_params
      params.require(:celulares).permit(:marca, :preco, :telefone, :descricao)
    end
		
		def set_users_celulares
			@celulares= current_user.celulares.find(params[:id])
		end
end
