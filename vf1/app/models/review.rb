class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :celulares, counter_cache: true

	validates_uniqueness_of :user_id, scope: :celulares_id
	validates_presence_of :points, :user_id, :celulares_id
	validates_inclusion_of :points, in: POINTS
end
